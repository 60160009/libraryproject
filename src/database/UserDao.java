/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author hafish
 */
public class UserDao {

    static ArrayList<User> list = new ArrayList<User>();

    public static boolean insert(User user) {
        Database.conn = Database.connectDB();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "INSERT INTO User (\n"
                    + "                     typeId,\n"
                    + "                     surname,\n"
                    + "                     name,\n"
                    + "                     password,\n"
                    + "                     loginName\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     %d,\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s'\n"
                    + "                 );";
            stm.executeQuery(String.format(sql, user.getTypeId(),
                    user.getSurname(), user.getName(), user.getPassword(),
                    user.getLoginName()));
            Database.closeDB();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static boolean update(User user) {
        Database.conn = Database.connectDB();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "UPDATE user set\n"
                    + "loginName = '%s',\n"
                    + "password = '%s',\n"
                    + "name = '%s' ,\n"
                    + "surname = '%s' ,\n"
                    + "typeId = %d \n"
                    + "WHERE userId = %d"
                    + "";
            String sqlFormat = String.format(sql,
                    user.getLoginName(),
                    user.getPassword(),
                    user.getName(),
                    user.getSurname(),
                    user.getTypeId(),
                    user.getUserId());

            stm.executeQuery(sqlFormat);
            Database.closeDB();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static boolean delete(User user) {
        Database.conn = Database.connectDB();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "DELETE FROM user\n"
                    + "WHERE userId = %d" ;
            String sqlFormat = String.format(sql,
                    user.getUserId());

            stm.executeQuery(sqlFormat);
            Database.closeDB();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static void getUser() {
        Database.conn = Database.connectDB();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "SELECT userId,\n"
                    + "       loginName,\n"
                    + "       password,\n"
                    + "       name,\n"
                    + "       surname,\n"
                    + "       typeId\n"
                    + "  FROM User";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("userId") + " " + rs.getString("loginName"));
                User user = toObject(rs);
                list.add(user);
            }
            Database.closeDB();
        } catch (SQLException e) {

        }
    }

    public static User getUser(int userId) {
        Database.conn = Database.connectDB();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "SELECT * FROM user WHERE userId = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            while (rs.next()) {
                User user = toObject(rs);
                Database.closeDB();
                return user;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setLoginName(rs.getString("loginName"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setPassword(rs.getString("password"));
        user.setTypeId(rs.getInt("typeId"));
        return user;
    }

}
