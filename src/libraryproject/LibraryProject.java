/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Database;
import database.User;
import database.UserDao;
import java.sql.Connection;


public class LibraryProject {

    static Connection conn = null;

    public static void main(String[] args) {
//        User newUser = new User();
//        newUser.setUserId(-1);
//        newUser.setLoginName("Lookkaew");
//        newUser.setPassword("Kaew2345678");
//        newUser.setName("Jatuporn");
//        newUser.setSurname("Hemranon");
//        newUser.setTypeId(1);
//        UserDao.insert(newUser);
//        UserDao.getUser();
        
        int userId = 2;
        User user = UserDao.getUser(userId);
        if (user == null) {
            System.out.println("Can't find User : " + userId);
        } else {
            System.out.println(user);
            System.out.println("Update");
        }
//        user.setName("LookKaew");
//        user.setLoginName("LisaYaaaaa");
//        UserDao.update(user);
        UserDao.delete(user);
        
        user = UserDao.getUser(userId);
        if (user == null) {
            System.out.println("Can't find User : " + userId);
        } else {
            System.out.println(user);
        }

    }
}
